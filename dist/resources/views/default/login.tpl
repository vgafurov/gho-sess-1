{extends file="index.tpl"}

{block name="title"}Авторизация - {/block}

{block name="breadcrumbs"}
<ol class="breadcrumb">
  <li><a href="{$prefix}/">Главная</a></li>
  <li class="active">Вход</li>
</ol>
{/block}

{block name="content"}{strip}
<div class="row">
    <div class="col col-md-6">
        <form method="post" action="{$prefix}/login/" name="frm-login">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" placeholder="user@main.com" class="form-control">
            </div>
            <div class="form-group">
                <label for="passwd">Пароль</label>
                <input type="password" name="passwd" placeholder="*********" class="form-control">
            </div>
            <button type="submit" class="btn btn-default">Войти</button>
        </form>
    </div>
    <div class="col col-md-6">
        {if isset($msg)}
        <p class="text-danger">{$msg}</p>
        {/if}
        <p>Только авторизованный персонал.</p>
    </div>
</div>
{/strip}{/block}