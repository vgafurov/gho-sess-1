{extends file="index.tpl"}

{block name="title"}Новый пост - {/block}

{block name="breadcrumbs"}
<ol class="breadcrumb">
  <li><a href="{$prefix}/">Главная</a></li>
  <li><a href="{$prefix}/posts/">Все посты</a></li>
  <li class="active">Новый пост</li>
</ol>
{/block}

{block name="content"}{strip}
<div class="row">
    <div class="col col-md-6">

        <h2>Новый пост</h2>
        <form method="post" action="{$prefix}/new-post/" name="frm-post">
            <div class="form-group">
                <label for="title">Название</label>
                <input type="text" name="title" placeholder="Название поста" required="required" class="form-control">
            </div>
            <div class="form-group">
                <label for="f_text">Текст</label>
                <textarea name="f_text" placeholder="Название поста" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-default">Сохранить</button>
        </form>
    </div>
    <div class="col col-md-6">
        {if isset($msg)}
        <p class="text-primary">{$msg}</p>
            {if isset($newId)}
            <p>Новый пост: <a href="{$prefix}/post/{$newId}/">перейти</a>.</p>
            {/if}
        {/if}
    </div>
</div>
{/strip}{/block}