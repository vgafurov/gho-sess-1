{extends file="index.tpl"}

{block name="content"}{strip}
<h1>Добро пожаловать в мой блог!</h1>
<div class="row">
    <div class="col col-md-4">
        <p>Функции</p>
        <ul>
            <li>Добавление постов</li>
            <li>Редактирование постов</li>
            <li>Удаление постов</li>
            <li>Просмотр постов</li>
        </ul>
    </div>
    <div class="col col-md-8">
        <p>Простейший блог, который можно создать за час, используя простые, но очень крутые технологии.</p>
        <p>Если вас заинтересовал этот урок, вступайте в группу школы веб-программирования «<a href="https://vk.com/wedev" rel="nofollow" target="_blank">WeDev</a>».</p>
    </div>
</div>
<div class="row">
    <h3>Последние записи</h3>
    {foreach from=$posts item=post}
    <div class="col col-md-4">
        <div class="thumbnail">
            <div class="caption">
                <h4><a href="{$prefix}/post/{$post.id}/">{$post.title}</a></h4>

                <div class="text-justify">
                    {$post.f_text|nl2br|truncate:150}
                </div>
                <div class="clearfix"></div>
                <p class="text-center"><a href="{$prefix}/post/{$post.id}/" class="btn btn-default">Прочитать полностью</a></p>
            </div>
        </div>
    </div>
    {/foreach}
</div>
{/strip}{/block}