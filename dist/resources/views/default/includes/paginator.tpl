{if isset($pages) && $pages > 1}
    <div class="paginator">
        <ul class="pagination">
            {* First/prev page *}
            {if $page == 1}
                <li class="disabled"><span>&#0171;</span></li>
            {else}
                <li><a href="{$preUrl}{$page - 1}/" data-page="{$page - 1}">&#0171;</a></li>
            {/if}

            {for $index=1 to $pages}
                {if $index == $page}
                    <li class="active"><a href="#">{$index}</a></li>
                {else}
                    <li><a href="{$preUrl}{$index}/" data-page="{$index}">{$index}</a></li>
                {/if}
            {/for}

            {* Next/last page *}
            {if $page == $pages}
                <li class="disabled"><span>&#0187;</span></li>
            {else}
                <li><a href="{$preUrl}{$page + 1}/" data-page="{$page + 1}">&#0187;</a></li>
            {/if}

        </ul>
    </div>
{/if}