{extends file="index.tpl"}

{block name="title"}Все посты - {/block}

{block name="breadcrumbs"}
<ol class="breadcrumb">
  <li><a href="{$prefix}/">Главная</a></li>
  <li class="active">Все посты</li>
</ol>
{/block}

{block name="content"}{strip}
<h2>Все посты</h2>
{foreach from=$posts item=post}
    <h3><a href="{$prefix}/post/{$post.id}/">{$post.title}</a></h3>

    <p class="text-right text-muted">{$post.cdate|date_format:'d M, D H:i'}</p>

    <div class="text-justify">
        {$post.f_text|nl2br|truncate:400}
    </div>
    <div class="clearfix"></div>

    <p class="text-center"><a href="{$prefix}/post/{$post.id}/" class="btn btn-default">Прочитать полностью</a></p>
    <hr>
{/foreach}

<div class="clearfix"></div>
{include file="includes/paginator.tpl"}
{/strip}{/block}