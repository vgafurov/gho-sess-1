{extends file="index.tpl"}

{block name="title"}{$post.title|htmlspecialchars} - {/block}

{block name="breadcrumbs"}
<ol class="breadcrumb">
  <li><a href="{$prefix}/">Главная</a></li>
  <li><a href="{$prefix}/posts/">Все посты</a></li>
  <li class="active">{$post.title}</li>
</ol>
{/block}

{block name="content"}{strip}
    <h3>{$post.title}</h3>

    <p class="text-right text-muted">{$post.cdate|date_format:'d M, D H:i'}</p>

    <div class="text-justify">
        {$post.f_text|nl2br}
    </div>
    <div class="clearfix"></div>

    {if $isAuthorized}
    <div class="row">
        <div class="col col-md-offset-8 col-md-4">
            <a href="{$prefix}/del-post/{$post.id}/" class="btn btn-danger btn-sm">
                <span class="glyphicon glyphicon-trash"></span> Удалить
            </a>
            &nbsp;
            <a href="{$prefix}/edit-post/{$post.id}/" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-pencil"></span> Редактировать
            </a>
        </div>
    </div>
    {/if}
    
    <div class="clearfix"></div>
    <hr>
    <a href="{$prefix}/posts/" class="btn btn-default">Все посты</a>
{/strip}{/block}