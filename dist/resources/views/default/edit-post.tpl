{extends file="index.tpl"}

{block name="title"}Редактирование поста - {/block}

{block name="breadcrumbs"}
<ol class="breadcrumb">
  <li><a href="{$prefix}/">Главная</a></li>
  <li><a href="{$prefix}/posts/">Все посты</a></li>
  <li><a href="{$prefix}/post/{$post.id|htmlspecialchars}/">{$post.title}</a></li>
  <li class="active">Редактирование поста</li>
</ol>
{/block}

{block name="content"}{strip}
<div class="row">
    <div class="col col-md-6">

        <h2>Новый пост</h2>
        <form method="post" action="/edit-post/" name="frm-post">
            <input type="hidden" name="id" value="{$post.id|htmlspecialchars}">
            <div class="form-group">
                <label for="title">Название</label>
                <input type="text" name="title" placeholder="Название поста" required="required" class="form-control" value="{$post.title|htmlspecialchars}">
            </div>
            <div class="form-group">
                <label for="f_text">Текст</label>
                <textarea name="f_text" placeholder="Название поста" class="form-control">{$post.f_text|htmlspecialchars}</textarea>
            </div>
            <a href="{$prefix}/post/{$post.id|htmlspecialchars}/" target="_blank" class="btn btn-info">Просмотреть</a>
            &nbsp;
            <button type="submit" class="btn btn-default">Сохранить</button>
        </form>
    </div>
    <div class="col col-md-6">
        {if isset($msg)}
        <p class="text-primary">{$msg}</p>
            {if isset($id)}
            <p>Обновлённый пост: <a href="{$prefix}/post/{$id|htmlspecialchars}/">перейти</a>.</p>
            {/if}
        {/if}
    </div>
</div>
{/strip}{/block}