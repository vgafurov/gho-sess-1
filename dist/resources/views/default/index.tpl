{strip}<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{block name="title"}{/block}Мой Блог</title>
    
    {* Normalize, Bootstrap и свой CSS *}
    <link rel="stylesheet" type="text/css" href="{$prefix}/resources/assets/normalize.css/normalize.css">
    <link rel="stylesheet" type="text/css" href="{$prefix}/resources/assets/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{$prefix}/web/css/main.css">
    
    {* jQuery, Bootstrap и свой JS *}
    <script src="{$prefix}/resources/assets/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="{$prefix}/resources/assets/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{$prefix}/web/js/main.js" type="text/javascript"></script>
</head>
<body>

{* Блок навигации *}
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
        {* Кнопка для мобильного меню *}
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <a class="navbar-brand" href="{$prefix}">Мой блог</a>
    </div>

    {* Навигационное меню *}
    <div class="collapse navbar-collapse" id="menu">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{$prefix}/posts/">Все Посты</a></li>
            {if $isAuthorized}
            <li><a href="{$prefix}/new-post/"><span class="glyphicon glyphicon-pencil"></span> <strong>Написать пост</strong></a></li>
            <li><a href="{$prefix}/logout/"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>
            {else}
            <li><a href="{$prefix}/login/"><span class="glyphicon glyphicon-log-in"></span> Войти</a></li>
            {/if}
        </ul>
    </div>
  </div>
</nav>

{* Основной двухколоночный блок *}
<div class="container">
    {block name="breadcrumbs"}{/block}
    <div class="row">
        <div class="col col-md-9">
            {/strip}{block name="content"}{/block}{strip}
        </div>
        <div class="col col-md-3">
            <p>Сайдбар, в котором можно что-нибудь разместить. Например, рекламу.</p>
        </div>
    </div>
</div>

{* Подвал *}
<footer class="footer">
    <div class="container">
        © Copyright, 2015
    </div>
</footer>
</body>
</html>
{/strip}