<?php
namespace wtsd\models;

use \wtsd\common\Database;

class Post
{
    protected $perPage = 3;

    public function count()
    {
        $sql = "SELECT count(*) AS `cnt` FROM `posts`";

        return Database::selectQuery($sql, null, true)['cnt'];
    }

    public function getPerPage()
    {
        return $this->perPage;
    }

    public function getLatest($count)
    {
        $placeholders = array(
            ':count' => array('type' => 'int', 'value' => $this->perPage),
            );

        $sql = "SELECT * FROM `posts` ORDER BY `cdate` DESC LIMIT :count";

        return Database::selectQueryBind($sql, $placeholders);
    }

    public function getAll($page = 1)
    {
        $offset = ($page - 1) * $this->perPage;

        $placeholders = array(
            ':offset' => array('type' => 'int', 'value' => $offset),
            ':count' => array('type' => 'int', 'value' => $this->perPage),
            );

        $sql = "SELECT * FROM `posts` ORDER BY `cdate` DESC LIMIT :offset, :count";

        return Database::selectQueryBind($sql, $placeholders);
    }

    public function getById($id)
    {
        $sql = "SELECT * FROM `posts` WHERE `id` = :id";
        $placeholders = array(':id' => $id);
        return Database::selectQuery($sql, $placeholders, true);
    }

    public function insert($title, $f_text)
    {
        $sql = "INSERT INTO `posts` SET `title` = :title, `f_text` = :f_text, `cdate` = Now()";
        $placeholders = array(
            ':title' => $title,
            ':f_text' => $f_text,
            );
        return Database::insertQuery($sql, $placeholders);
    }

    public function delete($id)
    {
        $sql = "DELETE FROM `posts` WHERE `id` = :id";
        $placeholders = array(':id' => $id);
        return Database::deleteQuery($sql, $placeholders);
    }

    public function update($id, $title, $f_text)
    {
        $sql = "UPDATE `posts` SET `title` = :title, `f_text` = :f_text WHERE `id` = :id";
        $placeholders = array(
            ':title' => $title,
            ':f_text' => $f_text,
            ':id' => $id,
            );
        Database::updateQuery($sql, $placeholders);
        return $id;
    }
}
