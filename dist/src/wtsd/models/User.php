<?php
namespace wtsd\models;

use \wtsd\common\Database;

class User
{
    public function isAuthenticated()
    {
        return (isset($_SESSION['is_authorized']) && $_SESSION['is_authorized']);
    }

    public function doAuthenticate($email, $password)
    {
        $sql = "SELECT * FROM `users` WHERE `email` = :email";
        $placeholders = array(':email' => $email);
        $row = Database::selectQuery($sql, $placeholders, true);
        //die(password_hash($password, CRYPT_BLOWFISH));
        if (password_verify($password, $row['passwd'])) {
            $_SESSION['is_authorized'] = true;
            $_SESSION['name'] = $row['name'];

            return true;
        }
        return false;
    }

    public function doLogout()
    {
        unset($_SESSION['is_authorized']);
        session_destroy();
    }
}
