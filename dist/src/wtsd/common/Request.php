<?php
namespace wtsd\common;

class Request
{
    
    public static function getPost($arg = '')
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST)) {
            $ret = $_POST;
            if ($arg === '') {
                return $ret;
            } elseif (array_key_exists($arg, $ret)) {
                return $ret[$arg];
            } else {
                return null;
            }
        } else { 
            return null;
        }
    }
    
    public static function getGet($arg = '')
    {
        $ret = $_GET;
        if ($arg === '') {
            return $ret;
        } elseif (array_key_exists($arg, $ret)) {
            return $ret[$arg];
        } else {
            return null;
        }
    }
    
    public static function parseUrl($index = null)
    {
        $url = $_SERVER['REQUEST_URI'];
        if ($index === null) {
            return $url;
        }
        $arr = array();
        $arr = explode("/", $url);
        array_shift($arr);

        if ((count($arr) > 0) 
            && ($index !== null && isset($arr[$index]))) {
            return $arr[$index];
        }
        return null;
    }

}
