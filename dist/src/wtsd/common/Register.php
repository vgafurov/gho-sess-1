<?php
namespace wtsd\common;

use wtsd\common;
use Symfony\Component\Yaml\Parser;
class Register
{

    protected static $register = array();

    private static $files;
    private static $instance = null;
    
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    protected static function includeFile($path, $type)
    {
        if (!file_exists($path)) {
            throw new \Exception('No configured ' . $path . ' file');
        } else {
            $yaml = new Parser();
            try {
                self::$register[$type] = $yaml->parse(file_get_contents($path));
                return self::$register[$type];
            } catch (ParseException $e) {
                die(sprintf("Unable to parse the YAML string: %s :: %s", $path, $e->getMessage()));
            }
        }
    }

    public function __construct()
    {
        $confDir = ROOT . DS . 'app' . DS . 'config' . DS;

        self::$files['config'] = $confDir .'config.yml';
        self::$files['db'] = $confDir . 'database.yml';
        self::$files['routing'] = $confDir . 'routing.yml';
        
    }

    static public function get($type, $field = null)
    {
        self::getInstance();
        if (isset(self::$register[$type])) {
            if ($field) {
                return self::$register[$type][$field];
            }
            return self::$register[$type];
        }

        if ($field) {
            return self::includeFile(self::$files[$type], $type)[$field];
        }
        return self::includeFile(self::$files[$type], $type);
    }

}
