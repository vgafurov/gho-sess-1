<?php
namespace wtsd\common;

class Controller
{
    protected $template = 'index.tpl';

    public function dispatch($action, $args = null)
    {
        session_start();
        try {
            header('Content-type: text/html; charset=utf-8');

            $contents = call_user_func_array(array(&$this, $action), array_values($args));
            $contents['uri'] = htmlspecialchars($_SERVER['REQUEST_URI'], ENT_QUOTES, 'utf-8');

            $contents['prefix'] = \wtsd\common\Register::get('config', 'prefix');

            $user = new \wtsd\models\User();
            $contents['isAuthorized'] = $user->isAuthenticated();

            $view = new \wtsd\common\Template(\wtsd\common\Register::get('config', 'template'));
            $view->assignAll($contents);
        
            $view->display($this->template);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }
}