<?php
namespace wtsd\common;

class Template
{
    protected $engine;
    protected $template;

    public function __construct($template = '')
    {
        $this->tmp_dir = ROOT . DS . 'temp' . DS;
        $this->view_dir = ROOT . DS;
        $this->template = $template;

        $this->engine = new \Smarty();
        $this->engine->caching = 0;
        $this->engine->config_dir = $this->view_dir . 'config';
        $this->engine->compile_dir = $this->tmp_dir;
        $this->engine->template_dir = $this->view_dir . $this->template;
        $this->engine->cache_dir = $this->tmp_dir;

    }
    
    public function get()
    {
        return $this->engine;
    }
    
    public function assignAll($content)
    {
        if (is_array($content)) {
            foreach ($content as $field => $value) {
                $this->engine->assign($field, $value);
            }
        }
    }

    public function assign($field, $value)
    {
        $this->engine->assign($field, $value);
    }

    public function render($page)
    {
        return $this->engine->fetch($page);
    }
    
    public function display($page)
    {
        $this->engine->display($page);
    }
}
