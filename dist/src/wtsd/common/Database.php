<?php
namespace wtsd\common;

use PDO;
use PDOException;

class Database
{
    public static $instance = null;
    
    public static function getInstance()
    {
        if (null === self::$instance) {
            try {

                $db = \wtsd\common\Register::get('db');

                $configObj = new \Doctrine\DBAL\Configuration();
                self::$instance = \Doctrine\DBAL\DriverManager::getConnection($db, $configObj);
                if (!self::$instance) {
                    throw new \PDOException();
                }
            } catch (\PDOException $e) {
                die($e->getMessage());
            }
        }
        return self::$instance;
    }

    static public function selectQuery($sql, $placeholders = null, $single = false)
    {
        try {
            self::getInstance();
            $stmt = self::$instance->prepare($sql);
            $stmt->execute($placeholders);

            if ($single) {
                return $stmt->fetch(\PDO::FETCH_ASSOC);
            } else {
                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }
        } catch (\PDOException $e) {
            if ($e->getCode() !== '42000') {
                die($e->getMessage());
            }
        }
    }

    static public function selectQueryBind($sql, $placeholders = null, $single = false)
    {
        try {
            self::getInstance();
            $stmt = self::$instance->prepare($sql);
            foreach ($placeholders as $name => $arr) {
                if ($arr['type'] == 'int') {
                    $type = \PDO::PARAM_INT;
                } else {
                    $type = \PDO::PARAM_STR;
                }
                $stmt->bindParam($name, $arr['value'], $type);
            }

            $stmt->execute();
            if ($single) {
                return $stmt->fetch(\PDO::FETCH_ASSOC);
            } else {
                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }
        } catch (\PDOException $e) {
            self::error($e->getMessage());
        }
    }

    static public function updateQuery($sql, $placeholders = null)
    {
        try {
            self::getInstance();
            $stmt = self::$instance->prepare($sql);

            $stmt->execute($placeholders);
            return true;
        } catch (\PDOException $e) {
            self::error($e->getMessage());
        }
    }

    static public function insertQuery($sql, $placeholders = null)
    {
        try {
            self::getInstance();
            $stmt = self::$instance->prepare($sql);

            $stmt->execute($placeholders);
            return self::$instance->lastInsertId();
        } catch (\PDOException $e) {
            if ($e->errorInfo[1] == 1062) {
                return 0;
            } else {
                self::error($e->getMessage());
            }
        }
    }

    static public function deleteQuery($sql, $placeholders = null)
    {
        try {
            self::getInstance();
            $stmt = self::$instance->prepare($sql);

            $stmt->execute($placeholders);
            return self::$instance->lastInsertId();
        } catch (\PDOException $e) {
            if ($e->errorInfo[1] == 1062) {
                return 0;
            } else {
                self::error($e->getMessage());
            }
        }
    }
    
}
