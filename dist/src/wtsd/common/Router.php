<?php
namespace wtsd\common;

class Router
{
    private static $routes = array();
    
    public function __construct()
    {

        $routing = Register::get('routing');
        foreach ($routing as $regex => $controller) {
            $this->route($regex, $controller);
        }
    }
    
    public function route($pattern, $callback)
    {
        $prefix = Register::get('config', 'prefix');
        $pattern = '/^' . str_replace('/', '\/', $prefix) . '' . str_replace('/', '\/', $pattern) . '$/';
        self::$routes[$pattern] = $callback;
    }
    
    public function execute($uri)
    {
        $url = explode('?', $uri)[0];
        foreach (self::$routes as $pattern => $callback) {
            if (preg_match($pattern, $url, $params)) {
                array_shift($params);
                $arr = explode('::', $callback);
                $arr[] = $params;

                return $arr;
            }
        }
        throw new \Exception('Route was not found');
    }

}