<?php
namespace wtsd\common;

class AppKernel
{
    public function runWeb()
    {
        date_default_timezone_set('Europe/Moscow');
        try {
            $router = new \wtsd\common\Router();
            list($ctlName, $method, $args) = $router->execute($_SERVER['REQUEST_URI']);
            $controller = new $ctlName();
            $controller->dispatch($method, $args);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }
}