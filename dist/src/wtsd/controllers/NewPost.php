<?php
namespace wtsd\controllers;

use wtsd\common;
use wtsd\common\Controller;
/**
* 
*
* @author    Vladislav Gafurov <warlockfx@gmail.com>
* @version    0.1
*/
class NewPost extends Controller
{

    protected $template = 'new-post.tpl';

    public function run()
    {
        $contents = array();
        $user = new \wtsd\models\User();
        
        if (!$user->isAuthenticated()) {
            header('Location: /', 301);
        }
        if (\wtsd\common\Request::getPost('title')) {
            $title = \wtsd\common\Request::getPost('title');
            $f_text = \wtsd\common\Request::getPost('f_text');

            $post = new \wtsd\models\Post();
            $id = $post->insert($title, $f_text);
            $contents['newId'] = $id;
            $contents['msg'] = 'Пост добавлен';
        }
        return $contents;
    }
    
}
