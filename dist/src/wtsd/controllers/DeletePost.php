<?php
namespace wtsd\controllers;

use wtsd\common;
use wtsd\common\Controller;
/**
* 
*
* @author    Vladislav Gafurov <warlockfx@gmail.com>
* @version    0.1
*/
class DeletePost extends Controller
{
    protected $template = 'del-post.tpl';

    public function run()
    {
        $contents = array();
        $user = new \wtsd\models\User();
        
        if (!$user->isAuthenticated()) {
            header('Location: /', 301);
        }
        if (\wtsd\common\Request::parseUrl(1)) {
            $id = \wtsd\common\Request::parseUrl(1);
            $post = new \wtsd\models\Post();
            $id = $post->delete($id);
            $contents['msg'] = 'Пост удалён!';
        }
        return $contents;
    }
    
}
