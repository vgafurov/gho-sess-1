<?php
namespace wtsd\controllers;

use wtsd\common;
use wtsd\common\Controller;
/**
* 
*
* @author    Vladislav Gafurov <warlockfx@gmail.com>
* @version    0.1
*/
class Logout extends Controller
{

    protected $template = 'logout.tpl';

    public function run()
    {
        $contents = array();

        $user = new \wtsd\models\User();
        $user->doLogout();
        
        return $contents;
    }
    
}
