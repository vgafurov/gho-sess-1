<?php
namespace wtsd\controllers;

use wtsd\common;
use wtsd\common\Controller;
/**
* 
*
* @author    Vladislav Gafurov <warlockfx@gmail.com>
* @version    0.1
*/
class ListPosts extends Controller
{

    protected $template = 'list-posts.tpl';

    public function run()
    {
        $contents = array();

        $postObject = new \wtsd\models\Post();
        $page = \wtsd\common\Request::parseUrl(1);
        $page = intval($page) == 0 ? 1 : $page;

        $contents['posts'] = $postObject->getAll($page);

        $contents['preUrl'] = '/posts/';
        $contents['page'] = $page;
        $contents['pages'] = ceil($postObject->count() / $postObject->getPerPage());
        
        return $contents;
    }
    
}
