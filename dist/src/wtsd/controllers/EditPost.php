<?php
namespace wtsd\controllers;

use wtsd\common;
use wtsd\common\Controller;
/**
* 
*
* @author    Vladislav Gafurov <warlockfx@gmail.com>
* @version    0.1
*/
class EditPost extends Controller
{

    protected $template = 'edit-post.tpl';

    public function run()
    {
        $contents = array();
        $user = new \wtsd\models\User();
        
        if (!$user->isAuthenticated()) {
            header('Location: /', 301);
        }
        $id = \wtsd\common\Request::parseUrl(1);

        $postObj = new \wtsd\models\Post();
        if (\wtsd\common\Request::getPost('id')) {
            $id = \wtsd\common\Request::getPost('id');
            $title = \wtsd\common\Request::getPost('title');
            $f_text = \wtsd\common\Request::getPost('f_text');

            
            $id = $postObj->update($id, $title, $f_text);
            $contents['msg'] = 'Пост обновлён';
        }

        $contents['post'] = $postObj->getById($id);
        $contents['id'] = $id;
        return $contents;
    }
    
}
