<?php
namespace wtsd\controllers;

use wtsd\common;
use wtsd\common\Controller;
/**
* 
*
* @author    Vladislav Gafurov <warlockfx@gmail.com>
* @version    0.1
*/
class Post extends Controller
{

    protected $template = 'post.tpl';

    public function run()
    {
        $contents = array();

        $postObj = new \wtsd\models\Post();
        $contents['post'] = $postObj->getById(\wtsd\common\Request::parseUrl(1));
        
        return $contents;
    }
    
}
