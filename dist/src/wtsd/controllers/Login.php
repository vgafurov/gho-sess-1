<?php
namespace wtsd\controllers;

use wtsd\common;
use wtsd\common\Controller;
/**
* 
*
* @author    Vladislav Gafurov <warlockfx@gmail.com>
* @version    0.1
*/
class Login extends Controller
{

    protected $template = 'login.tpl';

    public function run()
    {
        $contents = array();

        $user = new \wtsd\models\User();
        
        if ($user->isAuthenticated()) {
            header('Location: /', 301);
        }
        if (\wtsd\common\Request::getPost('email')) {
            $email = \wtsd\common\Request::getPost('email');
            $passwd = \wtsd\common\Request::getPost('passwd');

            if ($user->doAuthenticate($email, $passwd)) {
                header('Location: /', 301);
            } else {
                $contents['msg'] = 'Ошибка авторизации!';
            }
        }
        return $contents;
    }
    
}
