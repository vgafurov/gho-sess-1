<?php
namespace wtsd\controllers;

use wtsd\common;
use wtsd\common\Controller;
use wtsd\common\Register;
/**
* 
*
* @author    Vladislav Gafurov <warlockfx@gmail.com>
* @version    0.1
*/
class Home extends Controller
{

    protected $template = 'home.tpl';

    public function run()
    {
        $contents = array();

        $post = new \wtsd\models\Post();
        $contents['posts'] = $post->getLatest(2);
        return $contents;
    }
    
}
