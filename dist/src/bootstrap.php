<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

define('ROOT', dirname(dirname(__FILE__)));

require_once(ROOT . '/vendor/autoload.php');
